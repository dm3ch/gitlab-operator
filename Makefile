
# Image URL to use all building/pushing image targets
IMG ?= controller:latest

VERSION ?= $(shell cat ./VERSION)
PKG_PATH ?= gitlab.com/gitlab-org/charts/components/gitlab-operator
BUILDTIME := $(shell date -u +%Y%m%d.%H%M%S)
LDFLAGS ?= -ldflags '-X ${PKG_PATH}/pkg/version.version=${VERSION} -X ${PKG_PATH}/pkg/version.buildtime=${BUILDTIME}'

all: test manager

# Run tests
test: dep generate fmt vet manifests
	go test $(LDFLAGS) ./pkg/... ./cmd/... -coverprofile cover.out

# Build manager binary
manager: dep generate fmt vet
	go build $(LDFLAGS) -o bin/manager $(PKG_PATH)/cmd/manager

docker-manager:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build $(LDFLAGS) -o bin/manager $(PKG_PATH)/cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install: manifests
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests
	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests:
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go all

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
generate:
	go generate ./pkg/... ./cmd/...

# Get dependencies
dep:
	dep ensure -v

# Build the docker image
docker-build: test
	docker build . -t ${IMG}
	@echo "updating kustomize image patch file for manager resource"
	sed -i 's@image: .*@image: '"${IMG}"'@' ./config/default/manager_image_patch.yaml

# Push the docker image
docker-push:
	docker push ${IMG}
