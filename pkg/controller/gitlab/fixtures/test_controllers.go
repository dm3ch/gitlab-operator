package fixtures

import (
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

type PrefixedTestReconciler struct {
	client.Client
	scheme *runtime.Scheme
}

func (r *PrefixedTestReconciler) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	return reconcile.Result{}, nil
}

func NewPrefixedTestReconciler(m manager.Manager) reconcile.Reconciler {
	return &PrefixedTestReconciler{
		Client: m.GetClient(),
		scheme: m.GetScheme(),
	}
}

func AddPrefixedTestReconciler(m manager.Manager, r reconcile.Reconciler) error {
	c, err := controller.New(
		"gitlab-controller-with-prefix",
		m,
		controller.Options{
			Reconciler: r,
		})
	if err != nil {
		return err
	}
	return c.Watch(
		&source.Kind{
			Type: &GitLab{},
		},
		&handler.EnqueueRequestForObject{},
	)
}
